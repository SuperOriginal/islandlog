package pw.soopr.islandlog;


import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class TaggedPlayer {

    private Player player;
    private Player enemyPlayer;
    private int taskid;
    private int countdown;

    public TaggedPlayer(Player player, Player enemyPlayer){
        this.player = player;
        this.enemyPlayer = enemyPlayer;
        resetCountdown();
        this.taskid = Bukkit.getScheduler().scheduleSyncRepeatingTask(IslandLog.i, new Runnable() {
            @Override
            public void run() {
                if(!isFarEnoughAway()){
                    if(countdown != IslandLog.i.getTime() * 10)resetCountdown();
                }else{
                    countdown--;
                }

                if(inSafeRegion()){
                    end();
                }

                if(countdown <= 0){
                    end();
                }
            }
        },5,2);
    }

    public boolean isFarEnoughAway(){
        int distSqrd = IslandLog.i.getDistance() * IslandLog.i.getDistance();
        return player.getLocation().distanceSquared(enemyPlayer.getLocation()) > distSqrd;
    }

    public void end(){
        player.sendMessage(IslandLog.i.outOfCombat);
        Bukkit.getScheduler().cancelTask(taskid);
        IslandLog.i.getTaggedPlayers().remove(player.getName());
    }

    public void resetCountdown(){
        this.countdown = IslandLog.i.getTime() * 10;
    }

    public void updateEnemy(Player enemyPlayer){
        this.enemyPlayer = enemyPlayer;
    }

    public int getTimeLeft(){
        return countdown / 10;
    }

    public boolean inSafeRegion(){
        for(String s : IslandLog.i.getRegionsExempt()){
            ApplicableRegionSet set = WorldGuardPlugin.inst().getRegionManager(player.getWorld()).getApplicableRegions(player.getLocation());
            for(ProtectedRegion region : set.getRegions()){
                if(region.getId().equalsIgnoreCase(s)) return true;
            }
        }
        return false;
    }

}
