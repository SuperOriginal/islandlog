package pw.soopr.islandlog;


import net.md_5.bungee.api.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import pw.soopr.islandlog.file.ResourceFile;
import pw.soopr.islandlog.file.ResourceReloadHook;
import pw.soopr.islandlog.file.YAMLFile;
import pw.soopr.islandlog.listener.DamageListener;
import pw.soopr.islandlog.listener.DeathListener;
import pw.soopr.islandlog.listener.DisconnectListener;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class IslandLog extends JavaPlugin{

    private YAMLFile config;

    private int time;
    private int distance;
    private List<String> regionsExempt;

    public static IslandLog i;

    private Map<String,TaggedPlayer> taggedPlayers;

    public String outOfCombat;
    public String killedPlayer;
    public String diedPlayer;
    public String youTagged;
    public String youWereTaggedBy;
    public String notFarEnough;
    public String timeLeft;
    public String notTagged;

    public void onEnable(){
        i = this;
        taggedPlayers = new HashMap<>();
        config = new YAMLFile(this, "config.yml", new ResourceReloadHook() {
            @Override
            public void onReload(ResourceFile file) {
                YAMLFile cf = (YAMLFile) file;
                time = cf.getConfig().getInt("time");
                distance = cf.getConfig().getInt("distance");
                regionsExempt = cf.getConfig().getStringList("regionsExempt");
                outOfCombat = color(cf.getConfig().getString("format.outOfCombat"));
                killedPlayer = color(cf.getConfig().getString("format.youKilledPlayer"));
                diedPlayer = color(cf.getConfig().getString("format.youDied"));
                youTagged = color(cf.getConfig().getString("format.youTagged"));
                youWereTaggedBy = color(cf.getConfig().getString("format.youWereTaggedBy"));
                notFarEnough = color(cf.getConfig().getString("format.notFarEnough"));
                timeLeft = color(cf.getConfig().getString("format.timeLeft"));
                notTagged = color(cf.getConfig().getString("format.notTagged"));
            }
        });

        config.reloadConfig();

        PluginManager pm = getServer().getPluginManager();
        pm.registerEvents(new DamageListener(),this);
        pm.registerEvents(new DeathListener(),this);
        pm.registerEvents(new DisconnectListener(),this);
    }

    public boolean onCommand(CommandSender sender, Command cmd, String lbl, String[] args){
        if(cmd.getName().equalsIgnoreCase("islandlog")){
            if(sender.hasPermission("islandlog.reload")){
                config.reloadConfig();
                sender.sendMessage(ChatColor.GREEN + "Reloaded IslandLog.");
                return true;
            }else{
                sender.sendMessage(ChatColor.RED + "You do not have permission to run this command.");
                return true;
            }
        }

        if(cmd.getName().equalsIgnoreCase("ct")){
            if(!(sender instanceof Player)){
                sender.sendMessage(ChatColor.RED + "Only players can run this command.");
                return true;
            }

            Player p = (Player) sender;

            if(!p.hasPermission("islanclog.ct")){
                p.sendMessage(ChatColor.RED + "You do not have permission to run this command.");
                return true;
            }

            if(isTagged(p)){
                if(getTag(p).isFarEnoughAway()){
                    p.sendMessage(IslandLog.i.timeLeft.replace("<time>",getTag(p).getTimeLeft() + ""));
                }else{
                    p.sendMessage(IslandLog.i.notFarEnough);
                }
            }else{
                p.sendMessage(IslandLog.i.notTagged);
            }

        }
        return true;
    }

    public boolean isTagged(Player p){
        return taggedPlayers.containsKey(p.getName());
    }

    public TaggedPlayer getTag(Player p){
        return taggedPlayers.containsKey(p.getName()) ? taggedPlayers.get(p.getName()) : null;
    }

    public String color(String s){
        return ChatColor.translateAlternateColorCodes('&',s);
    }

    public void registerTag(Player player, Player enemyPlayer){
        taggedPlayers.put(player.getName(), new TaggedPlayer(player,enemyPlayer));
    }

    public void UnregisterTag(Player p){
        taggedPlayers.get(p.getName()).end();
    }

    public int getTime() {
        return time;
    }

    public int getDistance() {
        return distance;
    }

    public List<String> getRegionsExempt() {
        return regionsExempt;
    }

    public Map<String, TaggedPlayer> getTaggedPlayers() {
        return taggedPlayers;
    }
}
