package pw.soopr.islandlog.listener;

import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import pw.soopr.islandlog.IslandLog;

/**
 * Created by David K. Johnson on 7/21/2015.
 */
public class DamageListener implements Listener{

    @EventHandler
    public void onDmg(EntityDamageByEntityEvent e){
        if(e.getEntity() instanceof Player){
            Player entity = (Player) e.getEntity();
            Player damager = null;
            if(e.getDamager() instanceof Player) damager = (Player) e.getDamager();
            if(e.getDamager() instanceof Projectile){
                Projectile projectile = (Projectile) e.getDamager();
                if(projectile.getShooter() instanceof Player) damager = (Player) projectile.getShooter();
            }

            if(damager != null){

                if(entity.hasPermission("islandlog.exempt") || damager.hasPermission("islandlog.exempt")) return;

                if(IslandLog.i.isTagged(entity)){
                    IslandLog.i.getTag(entity).updateEnemy(damager);
                }else{
                    entity.sendMessage(IslandLog.i.youWereTaggedBy.replace("<name>",damager.getName()));
                    IslandLog.i.registerTag(entity,damager);
                }

                if(IslandLog.i.isTagged(damager)){
                    IslandLog.i.getTag(damager).updateEnemy(entity);
                }else{
                    damager.sendMessage(IslandLog.i.youTagged.replace("<name>",entity.getName()));
                    IslandLog.i.registerTag(damager,entity);
                }
            }
        }
    }
}
