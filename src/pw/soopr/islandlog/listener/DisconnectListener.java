package pw.soopr.islandlog.listener;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;
import pw.soopr.islandlog.IslandLog;

/**
 * Created by David K. Johnson on 7/21/2015.
 */
public class DisconnectListener implements Listener{
    @EventHandler
    public void onQuit(PlayerQuitEvent e){
        if(IslandLog.i.isTagged(e.getPlayer())){
            e.getPlayer().setHealth(0);
            IslandLog.i.UnregisterTag(e.getPlayer());
        }
    }
}
