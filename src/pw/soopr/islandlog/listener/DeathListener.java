package pw.soopr.islandlog.listener;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import pw.soopr.islandlog.IslandLog;

/**
 * Created by David K. Johnson on 7/21/2015.
 */
public class DeathListener implements Listener{

    @EventHandler
    public void onDeath(PlayerDeathEvent e){
        if(e.getEntity().getKiller() != null){
            Player killer = e.getEntity().getKiller();

            e.getEntity().sendMessage(IslandLog.i.diedPlayer.replace("<name>",killer.getName()));
            killer.sendMessage(IslandLog.i.killedPlayer.replace("<name>",e.getEntity().getName()));
        }
    }
}
